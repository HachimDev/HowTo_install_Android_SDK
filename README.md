# How to : Install Android SDK on Ubuntu 16.04


Download the command line tools for linux from [here](https://developer.android.com/studio/index.html)

Extract it to */home/android*


### 1. install the runtime libraries for the ia32/i386 architecture (64bits)
<code>sudo apt-get install lib32z1 lib32ncurses5 libbz2-1.0:i386 libstdc++6:i386

### 2. Install the G++ compiler :
<code>sudo apt-get install g++


### 3. Install JDK 8 or a later stable official release :
<code>sudo apt-get install python-software-properties
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

Set the JAVA_HOME :
<code>export JAVA_HOME=$(update-alternatives --query javac | sed -n -e 's/Best: *\(.*\)\/bin\/javac/\1/p')

### 4. Persist the new environments variables
edit the .bashrc file :  
<code>sudo su
chown -R root:root /home/android
chmod -R +xr /home/android
su magnusdev1
nano ~/.bashrc  
  
add these 2 lines :  
<code>export PATH=/android/tools:/android/platform-tools:$PATH
export ANDROID_HOME=/home/magnusdev1/home/android/

CTRL+X to save and exit

Load the new settings  
<code>bash

### 5. Install the APIs

#### start the Android SDK Manager : 
<code>cd /home/android/tools
sudo su -c "./android sdk"


select the latest API  
select the API 22  
in Tools, select :  

 - Android SDK Tools
 - Android SDK Platform-tools
 - Android SDK Build-tools

Select All Extras packages
**Install the packages**


### 6. Install openGL :
#### Install Synaptic
<code>sudo apt-get install synaptic
#### Open Synaptic Package Manager
search for __*opengl*__  
select :

 - libgles1-mesa-dev
 - libgles2-mesa-dev
 - libgles1-mesa
 - libgles2-mesa
 - libqt4-opengl [upgrade]
 - glmark2-es2
 - libgles1-mesa-dbg (may not be found)
 - libgles2-mesa-dbg (may not be found)
 - freeglut3
 - libhugs-opengl-bundled


### 7. Check that everything is working
in the Android SDK Manager :
**Tools** tab > Manage AVDs... > Device definitions > choose a device > Create AVD

Target > the API level in the target  
CPU/ABI > Google APIs Intel Atom (x86_64)  
Skin > No Skin  

in **Android Virtual Devices** tab :  
start your newly created emulator  


Done !
